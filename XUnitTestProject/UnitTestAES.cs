﻿using System;
using Xunit;

namespace XUnitTestProject
{
    public class UnitTestAES
    {
        ListPhrase listPhrase = new ListPhrase();
        ListPasswords listPasswords = new ListPasswords();
        string EncryptionKey = "{C4437E85-24D4-4191-8987-9DA018E49F18}";

        [Fact]
        public void TestInPhraseList()
        {
            foreach (var phrase in listPhrase.ListPhrases)
            {
                var enc = CryptographyPortableLibraries.DataCrypt.AesEncrypt(phrase, EncryptionKey);
                var dec = CryptographyPortableLibraries.DataCrypt.AesDecrypt(enc, EncryptionKey);
                Console.WriteLine("Phrase ENC: {0}", enc);
                Assert.True(phrase == dec, "Phrase OK ");
            }
        }

        [Fact]
        public void TestInPasswordList()
        {
            foreach (var pwd in listPasswords.ListPwds)
            {
                var enc = CryptographyPortableLibraries.DataCrypt.AesEncrypt(pwd, EncryptionKey);
                var dec = CryptographyPortableLibraries.DataCrypt.AesDecrypt(enc, EncryptionKey);
                Console.WriteLine("PWD ENC: {0}", enc);
                Assert.True(pwd == dec, "Pwd OK ");
            }
        }
    }
}
