﻿using System;
using Xunit;

namespace XUnitTestProject
{
    public class UnitTestSHA512
    {
        ListPhrase listPhrase = new ListPhrase();
        ListPasswords listPasswords = new ListPasswords();
        [Fact]
        public void TestInPhraseList()
        {
            foreach (var phrase in listPhrase.ListPhrases)
            {
                var enc1 = CryptographyPortableLibraries.DataCrypt.HashSHA512(phrase);
                var enc2 = CryptographyPortableLibraries.DataCrypt.HashSHA512(phrase);
                Console.WriteLine("Phrase ENC: {0}", enc1);
                Assert.True(enc1 == enc2, "Phrase OK ");
            }
        }

        [Fact]
        public void TestInPasswordList()
        {
            foreach (var pwd in listPasswords.ListPwds)
            {
                var enc1 = CryptographyPortableLibraries.DataCrypt.HashSHA512(pwd);
                var enc2 = CryptographyPortableLibraries.DataCrypt.HashSHA512(pwd);
                Console.WriteLine("PWD ENC: {0}", enc1);
                Assert.True(enc1 == enc2, "Pwd OK ");
            }
        }
    }
}
