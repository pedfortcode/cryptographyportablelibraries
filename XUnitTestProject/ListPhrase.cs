﻿using System.Collections.Generic;
namespace XUnitTestProject
{
    class ListPhrase
    {
        public List<string> ListPhrases { get; set; }

        public ListPhrase()
        {
            // by  https://www.csgenerator.com/pt
            ListPhrases = new List<string>();

            ListPhrases.Add("O herói misterioso voltou a fazer um salvamento.");
            ListPhrases.Add("Uma pequena relíquia descoberta incrível e misteriosamente intacta.");
            ListPhrases.Add("Os espelhos permitem-nos ver objectos que não conseguimos ver directamente.");
            ListPhrases.Add("Note que a vulnerabilidade é no processo de geração de chaves.");
            ListPhrases.Add("Ele tinha ordens para observar, mas não interferir.");
            ListPhrases.Add("Você ainda será vulnerável à vigilância direcionada.");
            ListPhrases.Add("O número de combinações possíveis e consequentes utilizações parece ilimitado.");
            ListPhrases.Add("A vossa coragem e sacrifício serão lembradas para todo o sempre.");
            ListPhrases.Add("A paz mundial não é somente possível, mas inevitável.");
            ListPhrases.Add("A vossa coragem e sacrifício serão lembradas para todo o sempre.");
            ListPhrases.Add("O número de combinações possíveis e consequentes utilizações parece ilimitado.");
            ListPhrases.Add("O laptop que você está usando e porquê?");
            ListPhrases.Add("Ele tinha ordens para examinar visualmente, mas não interferir.");
            ListPhrases.Add("Por favor, leia escrupulosamente as informações a seguir.");
            ListPhrases.Add("O herói críptico voltou a fazer um salvamento.");
            ListPhrases.Add("Ele irá abreviar o tempo indispensável para o nosso programa na inicialização.");
            ListPhrases.Add("Temos que escrutinar este caso.");
            ListPhrases.Add("O número de combinações possíveis e consequentes utilizações parece interminável.");
            ListPhrases.Add("Os espelhos sancionam-nos perceber visualmente objectos que não conseguimos discernir opticamente directamente.");
            ListPhrases.Add("Uma diminuta relíquia descoberta incrível e enigmaticamente intacta.");
            ListPhrases.Add("Note que a susceptibilidade é no processo de geração de chaves.");
            ListPhrases.Add("O herói inescrutável voltou a fazer um salvamento.");
            ListPhrases.Add("É possível, mas achamos bastante escéptico.");
            ListPhrases.Add("As pessoas devem observar opticamente mim para a consistência e perseverança.");
            ListPhrases.Add("Os espelhos sancionam-nos perceber visualmente objectos que não conseguimos discernir opticamente directamente.");
            ListPhrases.Add("Áreas plantadas são protegidos por barreiras de onda para abreviar a erosão.");
            ListPhrases.Add("A vossa intrepidez e sacrifício serão lembradas para todo o sempre.");
            ListPhrases.Add("É possível, mas achamos bastante dúbio.");
            ListPhrases.Add("Ficaremos astronomicamente exultantes se cantar alguma coisa.");
            ListPhrases.Add("A paz mundial não é somente possível, mas inevitavelmente inelutável.");
            ListPhrases.Add("Os espelhos sancionam-nos discernir opticamente objectos que não conseguimos discernir opticamente directamente.");
            ListPhrases.Add("Só é preciso uma centelha.");
            ListPhrases.Add("Ele tinha ordens para discernir opticamente, mas não interferir.");
            ListPhrases.Add("O número de combinações possíveis e consequentes utilizações parece ilimitável.");
            ListPhrases.Add("Ele irá truncar o tempo indispensável para o nosso programa na inicialização.");
            ListPhrases.Add("Note que a susceptibilidade é no processo de geração de chaves.");
            ListPhrases.Add("Só é preciso uma centelha.");
            ListPhrases.Add("Só achei que daria uma história intrigante.");
            ListPhrases.Add("As pessoas devem observar opticamente mim para a consistência e perseverança.");
            ListPhrases.Add("A vossa intrepidez e sacrifício serão lembradas para todo o sempre.");
            ListPhrases.Add("Até agora, tenho estado a evitar a sua inelutibilidade.");
            ListPhrases.Add("Por favor, leia conscienciosamente as informações a seguir.");
            ListPhrases.Add("Uma diminuta relíquia descoberta incrível e abstrusamente intacta.");
            ListPhrases.Add("O número de combinações possíveis e consequentes utilizações parece ilimitável.");
            ListPhrases.Add("Você ainda será susceptível à vigilância direcionada.");
            ListPhrases.Add("Ele tinha ordens para discernir opticamente, mas não interferir.");
            ListPhrases.Add("Por favor, leia meticulosamente as informações a seguir.");
            ListPhrases.Add("Por favor, leia escrupulosamente as informações a seguir.");
            ListPhrases.Add("A paz mundial não é somente possível, mas inevitavelmente inelutável.");
            ListPhrases.Add("Temos que escrutinar este caso.");
            ListPhrases.Add("Note que a susceptibilidade é no processo de geração de chaves.");
            ListPhrases.Add("Por favor, leia meticulosamente as informações a seguir.");
            ListPhrases.Add("O laptop que você está usando e para qual propósito?");
            ListPhrases.Add("O herói abstruso voltou a fazer um salvamento.");
            ListPhrases.Add("Temos que escrutinar este caso.");
            ListPhrases.Add("É possível, mas achamos bastante escéptico.");
            ListPhrases.Add("O herói críptico voltou a fazer um salvamento.");
            ListPhrases.Add("A paz mundial não é somente possível, mas inevitavelmente inelutável.");
            ListPhrases.Add("Uma diminuta relíquia descoberta incrível e abstrusamente intacta.");
            ListPhrases.Add("A paz mundial não é somente possível, mas inevitavelmente inelutável.");
            ListPhrases.Add("Por favor, leia escrupulosamente as informações a seguir.");
            ListPhrases.Add("O herói inescrutável voltou a fazer um salvamento.");
            ListPhrases.Add("As pessoas devem observar opticamente mim para a consistência e perseverança.");
            ListPhrases.Add("Ele tinha ordens para discernir opticamente, mas não interferir.");
            ListPhrases.Add("Os espelhos sancionam-nos perceber visualmente objectos que não conseguimos perceber visualmente directamente.");
            ListPhrases.Add("O número de combinações possíveis e consequentes utilizações parece ilimitável.");
            ListPhrases.Add("Você ainda será susceptível à vigilância direcionada.");
            ListPhrases.Add("Por favor, leia conscienciosamente as informações a seguir.");
            ListPhrases.Add("Áreas plantadas são protegidos por barreiras de onda para truncar a erosão.");
            ListPhrases.Add("O laptop que você está usando e para qual propósito?");
            ListPhrases.Add("Uma minúscula relíquia descoberta incrível e enigmaticamente intacta.");
        }
    }
}
