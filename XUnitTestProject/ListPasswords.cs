﻿using System.Collections.Generic;

namespace XUnitTestProject
{
    class ListPasswords
    {
        public List<string> ListPwds { get; set; }
        public ListPasswords()
        {
            // by https://www.lastpass.com/pt/password-generator
            ListPwds = new List<string>();

            ListPwds.Add("eSGP@Ok9KB!IT!E");
            ListPwds.Add("JzP7M%bAB0xT@g@");
            ListPwds.Add("#0OGm0aOiWeRfjl");
            ListPwds.Add("aP46IkfhKtBNx&I");
            ListPwds.Add("*@SC1o*Z1LKT1tc");
            ListPwds.Add("TDbP%GD9Pr!iv&5");
            ListPwds.Add("8&WU@ABZpSJ*L!!");
            ListPwds.Add("EePyO0C@AgnEmx3");
            ListPwds.Add("OCQTjMSwC*asorJ");
            ListPwds.Add("AetCcaL^weJZDE^");
            ListPwds.Add("!Mz@QidICrr@9q*");
            ListPwds.Add("#4ITi*NOoJV48Cc");
            ListPwds.Add("PRmh3@kzR5%TkYc");
            ListPwds.Add("!uaSGeeA65&nJ*T");
            ListPwds.Add("usdj$#Vi%UztKI1");
            ListPwds.Add("50df3MPp2zb@pvb");
            ListPwds.Add("8%5k^IePCDjL4U%");
            ListPwds.Add("Omd*27zQJJU%8fV");
            ListPwds.Add("!n6vZpcfnpgMR$l");
            ListPwds.Add("Eu5ffF*ax$3#!!L");
            ListPwds.Add("k*JUbT&iHwmjnHR");
            ListPwds.Add("7eN&&9@PHjQ0zeR");
            ListPwds.Add("iR7i3Z#DcpR&SkR");
            ListPwds.Add("ph#y&BF*YpAxb%V");
            ListPwds.Add("zHaXeetA&!LHK2u");
            ListPwds.Add("gG@AcEO^VMApD&r");
            ListPwds.Add("w@S57sOjSri8ai!");
            ListPwds.Add("LG84#mGp*7glylX");
            ListPwds.Add("8pZFO@kot#KFx^s");
            ListPwds.Add("6!!qpRtA%aj7Rmz");
        }
    }
}
