﻿using System;
using System.IO;
using System.Security.Cryptography;
using System.Text;

namespace DataCrypt
{
    /// <summary>
    /// Utility class for data encryption
    /// </summary>
    public static class DataCrypt
    {
        private const string ENCRYPTION_KEY = "437493A3-E129-47E3-8991-2E31093ADD3D";
        private readonly static byte[] SALT = Encoding.ASCII.GetBytes(ENCRYPTION_KEY.Length.ToString());

        #region Public methods for string Encryption and Decryption

        /// <summary>
        /// Encripta um valor em formato string
        /// </summary>
        /// <param name="value">A string a ser encriptada</param>
        /// <returns>A representação em string criptografada</returns>
        public static string EncryptData(string value)
        {
            if (String.IsNullOrEmpty(value))
            {
                throw new Exception("The value is null or empty");
            }

            if (value.Length > 255)
            {
                throw new Exception("The value must have less then 255 chars");
            }

            RijndaelManaged rijndaelCipher = new RijndaelManaged();
            byte[] plainText = Encoding.Unicode.GetBytes(value);
            PasswordDeriveBytes SecretKey = new PasswordDeriveBytes(ENCRYPTION_KEY, SALT);

            using (ICryptoTransform encryptor = rijndaelCipher.CreateEncryptor(SecretKey.GetBytes(32), SecretKey.GetBytes(16)))
            {
                using (MemoryStream memoryStream = new MemoryStream())
                {
                    using (CryptoStream cryptoStream = new CryptoStream(memoryStream, encryptor, CryptoStreamMode.Write))
                    {
                        cryptoStream.Write(plainText, 0, plainText.Length);
                        cryptoStream.FlushFinalBlock();
                        return Convert.ToBase64String(memoryStream.ToArray());
                    }
                }
            }
        }

        /// <summary>
        /// Decripta um valor em formato string
        /// </summary>
        /// <param name="encryptedValue">O valor criptografado</param>
        /// <returns></returns>
        public static string DecryptData(string encryptedValue)
        {
            RijndaelManaged rijndaelCipher = new RijndaelManaged();
            byte[] encryptedData = Convert.FromBase64String(encryptedValue);
            PasswordDeriveBytes secretKey = new PasswordDeriveBytes(ENCRYPTION_KEY, SALT);

            using (ICryptoTransform decryptor = rijndaelCipher.CreateDecryptor(secretKey.GetBytes(32), secretKey.GetBytes(16)))
            {
                using (MemoryStream memoryStream = new MemoryStream(encryptedData))
                {
                    using (CryptoStream cryptoStream = new CryptoStream(memoryStream, decryptor, CryptoStreamMode.Read))
                    {
                        byte[] plainText = new byte[encryptedData.Length];
                        int decryptedCount = cryptoStream.Read(plainText, 0, plainText.Length);
                        return Encoding.Unicode.GetString(plainText, 0, decryptedCount);
                    }
                }
            }
        }

        #endregion
    }
}
